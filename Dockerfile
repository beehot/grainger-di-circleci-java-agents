ARG DOCKER_REGISTRY=709741256416.dkr.ecr.us-east-1.amazonaws.com
FROM ${DOCKER_REGISTRY}/grainger-di-circleci-base-agent:5

LABEL maintainers = "grainger di"

ARG JDK_VERSION

# openjdk12, openjdk13, and openjdk14 only available in 'edge/testing'
RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
    apk update && apk add --no-cache \
    openjdk${JDK_VERSION}


ENV JAVA_HOME /usr/lib/jvm/java-${JDK_VERSION}-openjdk

RUN echo "Updating Java symlink" && unlink /usr/bin/java && ln -s ${JAVA_HOME}/bin/java /usr/bin/java

HEALTHCHECK NONE
