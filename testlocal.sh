#!/usr/bin/env bash
set -euo pipefail

CID="$(docker run -it -d --entrypoint ash local/grainger-di-circleci-gradle-java-11-agent:latest)"
inspec exec --no-distinct-exit profiles/cis-docker
inspec exec profiles/grainger-di-circleci-gradle-java-11-agent/ -t docker://$CID
docker rm -f $CID
