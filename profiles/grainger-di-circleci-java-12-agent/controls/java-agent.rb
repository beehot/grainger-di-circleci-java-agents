control 'java version' do
  impact 1.0
  title 'confirm java version installed'
  desc 'confirm version installed'
  describe command('java --version') do
    its('stdout') { should include ('12.0') }
  end
end
