control 'java version' do
  impact 1.0
  title 'confirm java version installed'
  desc 'confirm version installed'
  describe command('java --version') do
    its('stdout') { should include ('11.0') }
  end
end

control 'aws version' do
  impact 1.0
  title 'confirm awscli and botocore versions support "$ aws codeartifact"'
  describe pip('awscli') do
    its('version') { should cmp >= '1.18.134' }
  end
  describe pip('botocore') do
    its('version') { should cmp >= '1.17.57' }
  end
end

