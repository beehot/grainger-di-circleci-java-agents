[![CircleCI](https://circleci.com/bb/wwgrainger/grainger-di-circleci-java-agents.svg?style=svg&circle-token=5bbb0875de9256e431be4a3b0e71e5fde3172011)](https://circleci.com/bb/wwgrainger/grainger-di-circleci-java-agents)

# grainger-di-circleci-java-agents

Builds multiple java agents

Agent Quay Repositories:

* [Java 11](https://quay.io/repository/grainger/grainger-di-circleci-java-11-agent)
* [Java 12](https://quay.io/repository/grainger/grainger-di-circleci-java-12-agent)
* [Java 13](https://quay.io/repository/grainger/grainger-di-circleci-java-13-agent)
* [Java 14](https://quay.io/repository/grainger/grainger-di-circleci-java-14-agent)

## Building and testing Locally

To build:
```
# All images
invoke buildlocal
# Specific jdk version
invoke buildlocal --jdk-version 11
```

To run inspec tests:
```
# Runs tests against all images
invoke testlocal
# Runs tests agains specific image
invoke testlocal --jdk-version 11
```

Individual inspec tests can be found in `profiles/<agent-directory>`
