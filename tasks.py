from invoke import task


@task
def enc(ctx, file='local.env', encoded_file='env.ci'):
    ctx.run("openssl aes-256-cbc -e -in {} -out {} -k $GRAINGER_CIRCLECI_ENC".format(file, encoded_file))

@task
def dec(ctx, encoded_file='env.ci', file='local.env'):
    ctx.run("openssl aes-256-cbc -d -in {} -out {} -k $GRAINGER_CIRCLECI_ENC".format(encoded_file, file))

@task
def buildlocal(ctx, jdk_version=None):
    for version in jdk_version_range(jdk_version):
        image_tag = f"local/grainger-di-circleci-java-{version}-agent:latest"
        print(f"****** Building local/grainger-di-circleci-java-{version}-agent:latest ******")
        ctx.run(f"""
            docker build \
                --force-rm \
                --build-arg JDK_VERSION={version} \
                -t {image_tag} \
                .
        """)

@task
def testlocal(ctx, jdk_version=None):
    for version in jdk_version_range(jdk_version):
        image = f"grainger-di-circleci-java-{version}-agent"
        image_tag = f"local/{image}:latest"

        print(f"****** Testing {image_tag} ******")
        container_id = ctx.run(f"docker run -it -d --entrypoint ash {image_tag}", hide="out").stdout
        ctx.run("inspec exec --no-distinct-exit profiles/cis-docker", hide="out")
        ctx.run(f"inspec exec profiles/{image}/ -t docker://{container_id}", hide="out")
        ctx.run(f"docker rm -f {container_id}", hide="out")
    
    print("Looks good!!")

def jdk_version_range(jdk_version):
    range_start = int(jdk_version) if jdk_version else 11
    range_end = range_start + 1 if jdk_version else 15
    return range(range_start, range_end)